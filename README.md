[![img](https://img.shields.io/badge/Made_in-Doom_Emacs-blue?style=for-the-badge)](https://github.com/hlissner/doom-emacs)
<a href="https://www.instagram.com/justinekizhak"><img src="https://i.imgur.com/G9YJUZI.png" alt="Instagram" align="right"></a>
<a href="https://twitter.com/justinekizhak"><img src="http://i.imgur.com/tXSoThF.png" alt="Twitter" align="right"></a>
<a href="https://www.facebook.com/justinekizhak"><img src="http://i.imgur.com/P3YfQoD.png" alt="Facebook" align="right"></a>

---

<h1>Zone-end-of-buffer</h1>

<!-- markdown-toc start - Don't edit this section. Run M-x markdown-toc-refresh-toc -->

**Table of Contents**

- [Introduction](#introduction)
  - [Screenshot](#screenshot)
- [Features](#features)
- [Getting started](#getting-started)
  - [Prerequisites](#prerequisites)
- [Full Documentation](#full-documentation)
- [License](#license)

<!-- markdown-toc end -->

# Introduction

Watch your [Emacs] "Zone out" with this awesome animation.
This package is shamelessly stolen from [Joseph Wilk].

[Zone mode] is a Emacs package to animate your text when idle.

## Screenshot

# Features

# Getting started

## Prerequisites

- [Zone mode]

1. Install [Zone mode].

2. Add these lines to install it.

```emacs-lisp
(with-eval-after-load 'zone-end-of-buffer
  (unless (memq 'zone-end-of-buffer (append zone-programs nil))
    (setq zone-programs
          (vconcat zone-programs [zone-end-of-buffer]))))
```

# Full Documentation

# License

Licensed under the terms of GPLv3

[emacs]: https://www.gnu.org/software/emacs/
[joseph wilk]: https://github.com/repl-electric/view-pane/
[zone mode]: https://www.emacswiki.org/emacs/ZoneMode
